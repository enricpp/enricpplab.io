## Enric Petrus Pérez

**Welcome to my personal web page.** My name is Enric Petrus Pérez and I am currently working as a postdoctoral researcher
in the Federal Institute of Aquatic Science, [Eawag](https://www.eawag.ch/de/), in Dübendorf (Switzerland). My research 
focuses on the construction of chemical reaction networks to predict chemical speciation and 
kinetics in aqueous systems. 

My doctoral thesis at [ICIQ](https://www.iciq.org/) focused on developing [POMSimulator](https://doi.org/10.1039/D0SC03530K), a method that
allows the prediction of speciation diagrams and self-assembly mechanisms of polyoxometalates in water. 
We have recently released the first open-source version in [Github](https://github.com/petrusen/pomsimulator) - for more details see the [documentation](https://pomsimulator.readthedocs.io/en/latest/).

I am also involved in outreach scientific activities. I performed a four minute 
[monologue](https://www.youtube.com/watch?v=afLn5BxYf7U) about my thesis, I talked about computational chemistry and artificial intelligence in two local programs
 [Ones de Ciència](http://radiocambrils.alacarta.cat/onesdeciencia/capitol/ones-de-ciencia-capitol-18-quimica-computacional-convidat-enric-petrus), 
and [Blau de Prússia](https://www.youtube.com/watch?v=6hzpMFlvtJI&list=PLUeoQoVHsaNTlzyLuHHFscVFihxWfdV5H), and 
I gave a talk in a European outreach activity [Nit de la Recerca](https://www.youtube.com/watch?v=EGGs6VePeJo).
 

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/img/outreach.jpg" caption="ACS Spring - Indianapolis 2023">}}
  {{< figure thumb="-thumb" link="/img/EPetrus_Thesis.jpg" caption="PhD Thesis 2022">}}
  {{< figure thumb="-thumb" link="/img/epetrus_radio.png" caption="Radio Program 2020">}}
{{< /gallery >}}
{{< load-photoswipe >}}
