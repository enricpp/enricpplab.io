---
title: Posts
subtitle: (updated March 31, 2021)
date: 2020-06-23
---

**Poster Presentation**\
Place: Riudoms (Spain) in 2018\
Title: Unveiling a Photoinduced Hydrogen Evolution Reaction Mechanism via the Concerted Formation of Uranyl Peroxide\
Conference: INTECAT School

**Poster Presentation**\
Place: Tarragona (Spain) in 2019\
Title: Unveiling a Photoinduced Hydrogen Evolution Reaction Mechanism via the Concerted Formation of Uranyl Peroxide\ 
Conference: ICIQ PhD Day III

**Poster Presentation**\
Place: Corvallis (United States) in 2019\
Title: Unveiling a Photoinduced Hydrogen Evolution Reaction Mechanism via the Concerted Formation of Uranyl Peroxide\
Conference: Frontiers in Metal Oxide Cluster Science VI (FMOCS)

**Oral Communication**\
Place: Sitges (Spain) in 2020\
Title: Performance of group additivity methods for predicting the stability of uranyl complexes Conference: 
Biannual conference of the Catalan Society of Chemistry (JIPC)\
Conference: Biannual conference of the Catalan Society of Chemistry (JIPC)

**Flash Presentation**\
Place: online (Spain) in 2020\
Title: Speciation and Self-Assembly Mechanism of Polyoxometalates\
Conference: ICIQ PhD Day IV

**Oral Communication**\
Place: Girona (Spain) in 2022\
Title: Mecanismes de Formació i Diagrames d’Especiació dels Òxids Metàl·lics Moleculars\
Conference: Biannual conference of the Catalan Society of Chemistry (JIPC)

**Oral Communication**\
Place: Rimini (Italy) in 2022\
Title: POMSimulator: a Method for Predicting the Speciation and Self-Assembly of Polyoxometalates\
Conference: International Conference of Coordination Chemistry (ICCC)

**Oral Communication**\
Place: Barcelona (Spain) in 2023\
Title: Predicting the aqueous speciation and self-assembly mechanisms of Mo, W, V, Nb and Ta polyoxometalates\
Conference: Catalan conference of inorganic chemistry and organometallics (RQIO)

**Oral Communication**\
Place: Indianapolis (United States) in 2023\ 
Title: Ten years of interplay between theory and experiments\
Conference: ACS Spring Meeting

**Oral Communication**\
Place: Tarragona (Spain) in 2023\
Title: Development of a Method for Predicting the Aqueous Speciation and Self-Assembly of Polyoxometalates\
Conference: Frontiers in Metal Oxide Cluster Science VII (FMOCS)

**Poster Presentation**\
Place: Bern (Switzerland) in 2023\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Conference: Swiss Chemical Society Fall Meeting (SCS)

**Poster Presentation**\
Place: Zürich (Switzerland) in 2023\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Conference: 59th Symposium on Theoretical Chemistry (STC)\

**Poster Presentation**\
Place: Dübendorf (Switzerland) in 2023\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Conference: Eawag Symposium

**Oral Communication**\
Place: Tarragona (Spain) in 2024\
Title: An Open-Source Methodology for Predicting the Aqueous Speciation: from Polyoxometalates to Environmental Chemistry\
Conference: Electronic Structure Principles and Applications (ESPA)
