---
title: Publications
subtitle: (updated September 4, 2024)
date: 2024-09-04
---

#### List of peer-reviewed publications 

1. Martin, N. P.; Petrus, E.; Segado, M.; Arteaga, A.; Zakharov, L. N.; Bo, C.;
Nyman, M. Strategic Capture of the {Nb7} Polyoxometalate. *Chem. - A Eur. J.*
 **2019**, 10580–10584. [DOI](https://doi.org/10.1002/chem.201902770)


2. Petrus, E.; Bo, C. Performance of Group Additivity Methods for Predicting the
Stability of Uranyl Complexes. *J. Comput. Chem.* **2020**, 41 (11), 1124–1129.
[DOI](https://doi.org/10.1002/jcc.26157)


3. Petrus, E.; Segado, M.; Bandeira, N. A. G.; Bo, C. Unveiling a Photoinduced
Hydrogen Evolution Reaction Mechanism via the Concerted Formation of Uranyl
Peroxide. *Inorg. Chem.* **2020**, 59 (12), 8353–8360.
[DOI](https://doi.org/10.1021/acs.inorgchem.0c00757)


4. Petrus, E.; Segado, M.; Bo, C. Nucleation Mechanisms and Speciation of Metal
Oxide Clusters. *Chem. Sci.* **2020**, 11, 8448-8456.
[DOI](https://doi.org/10.1039/D0SC03530K); *Preprint Available
at [chemrxiv](https://doi.org/10.26434/chemrxiv.12639269.v1)*


5. Petrus, E.; Bo, C. Unlocking Phase Diagrams for Molybdenum and Tungsten Nanoclusters and
Prediction of their Formation Constants. *J. Phys. Chem. A* **2021**, 125, 23, 5212–5219. [DOI](https://doi.org/10.1021/acs.jpca.1c03292);
 *Preprint Available at [chemrxiv](https://doi.org/10.26434/chemrxiv.14292119)*


6. Rahman, T.; Petrus, E.; Segado,  M.; Martin, N. P.; Palys, L. N.; Rambaran, M. A.;
Ohlin, A.; Bo, C.; Nyman, M. Predicting the Solubility of Inorganic Ion Pairs in Water
*Angew. Chem. Int. Ed.* **2022**, 61, e202117839; [DOI](https://doi.org/10.1002/anie.202117839)


7. Petrus, E.; Segado, M.; Bo, C. Computational Prediction of Speciation Diagrams and Nucleation 
Mechanisms: Molecular Vanadium, Niobium and Tantalum Oxide Nanoclusters in Solution *Inorg. Chem.* **2022**, 61, 35, 13708–13718.
[DOI](https://doi.org/10.1021/acs.inorgchem.2c00925); *Preprint Available
at [chemrxiv](https://doi.org/10.26434/chemrxiv-2022-2pt95)*


8. Garay-Ruiz, D.; Petrus, E.; Bo, C. New graph-based computational methods for dealing with chemical reactivity and catalysis
*Revista de la Societat Catalana de Quimica*, **2023**, 22, 23-38 [DOI](https://revistes.iec.cat/index.php/RSCQ/article/view/150830/148565) 


9.  Petrus, E.; Garay-Ruiz, D.; Reiher, M.; Bo, C. Multi-Time-Scale Simulation of Complex Reactive Mixtures: How Do Polyoxometalates Form?
*J. Am. Chem. Soc.* **2023**, 145, 34, 18920-18930; [DOI](https://pubs.acs.org/10.1021/jacs.3c05514)


10. Tamai, N.; Ogiwara, N.; Hayashi, E.; Kamata, K.; Misaya, T.; Ito, T.; Kojima, T.; Segado, M.;
Petrus, E.; Bo, C.; Uchida, S. A redox-active inorganic crown ether based on a polyoxometalate capsule
*Chem. Sci.* **2023**, 14, 20, 5453-5459; [DOI](https://pubs.rsc.org/en/content/articlepdf/2023/sc/d3sc01077e)


11.  Petrus, E.; Buils, J.; Garay-Ruiz, D.; Segado-Centellas, M.; Bo, C. POMSimulator: An open-source tool for predicting the aqueous speciation and self–assembly mechanisms of polyoxometalates 
*J. Comput. Chem.* **2024**, 45, 2242–2250; [DOI](https://onlinelibrary.wiley.com/doi/10.1002/jcc.27389)


12. Buils, J.; Garay-Ruiz, D.; Segado-Centellas, M.; Petrus, E.; Bo, C. Computational insights into aqueous speciation of metal-oxide nanoclusters: an in-depth study of the Keggin phosphomolybdate 
*Chem. Sci.* **2024**, 15, 14218-14227; [DOI](https://pubs.rsc.org/en/content/articlelanding/2024/sc/d4sc03282a)


13. Buils, J.; Garay-Ruiz, D.; Petrus, E.; Segado-Centellas, M.; Bo, C. Towards a Universal Scaling Method for Predicting Equilibrium Constants of Polyoxometalates
*Chem. Sci.* **2024**, [ChemRxiv](https://doi.org/10.26434/chemrxiv-2024-r2lsq) 
