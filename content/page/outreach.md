---
title: Outreach and Community Work
subtitle: (updated November 21, 2022)
date: 2020-06-23
---

### Outreach and Community Work
\
**Scientific Monologue about the PhD research**\
Place: Tarragona (Spain) in 2019\
Accessible in: youtube.com/watch?v=afLn5BxYf7U (accessed 10/03/23)\
Institution: Roviri i Virgili University (URV)

**Radio Program about Computational Chemistry**\
Place: Cambrils (Spain) in 2020\
Accessible in: radiocambrils_podcast_2244 (accessed 10/03/23)\
Institution: Radio Cambrils

**Community Manager of Twitter Research Account**\
Place: Tarragona (Spain) from 2020 to 2022\
Accessible in: twitter.com/HartreeFoca (accessed 30/04/24)\
Institution: Catalan Institute of Chemical Research (ICIQ)

**Conference Organizer of the Vth ICIQ PhD Day**\
Place: Tarragona (Spain) in 2021\
Institution: Catalan Institute of Chemical Research (ICIQ)

**Outreach Talk about Machine Learning**\
Place: Reus (Spain) in 2022\
Accessible in: youtube.com/watch?v=EGGs6VePeJo&list (accessed 10/03/23)\
Institution: European Night of Research

**Computational Chemistry Workshop Organizer**\
Place: Tarragona (Spain) in 2023\
Institution: Newcastle University – John Errington research group

**Peer-Reviewer**\
Date: From 2022 onwards\
Journals: Journal of Computational Chemistry (Wiley) and Inorganic Chemistry (ACS) 

**Release of an open-access version of POMSimulator**\
Date: February 2024\
Source: github.com/petrusen/pomsimulator
