---
title: Awards and Honors
subtitle: (updated August 28, 2024)
date: 2024-08-28
---

### Awards and Honors 
\
**Special Doctoral Prize in Chemistry**\
Place: Tarragona (Spain) in 2024\
Institution: Roviri i Virgili University (URV)

**First Award for the III Computational Methodology Prize**\
Place: Madrid (Spain) in 2023\
Institution: Chemistry Computation Division of the Spanish Royal Society of Chemistry (REQC)

**First Award for the Poster Presentation at Eawag Symposium**\
Place: Dübendorf (Switzerland) in 2023\
Institution: Swiss Federal Institute of Aquatic Research (Eawag)

**Cover Art for the J. Am. Chem. Soc. article**\
Place: Tarragona (Spain) in 2023\
Institution: American Chemical Society (ACS)

**Cum Laude Mention and International Doctoral Mention**\
Place: Tarragona (Spain) in 2022\
Institution: Roviri i Virgili University (URV)

**Fellowship Severo Ochoa for the PhD Stay at ETH Zürich**\
Place: Tarragona (Spain) in 2021\
Institution: Catalan Institute of Chemical Research (ICIQ)

**Cover Art for the J. Phys. Chem. A. article**\
Place: Tarragona (Spain) in 2021\
Institution: American Chemical Society (ACS)

**First Award for the Best Oral Communication at PhD Day IV**\
Place: Tarragona (Spain) in 2020\
Institution: Catalan Institute of Chemical Research (ICIQ)

**First Award for the Scientific Monologue Competition**\
Place: Tarragona (Spain) in 2019\
Institution: Roviri i Virgili University (URV)

**Second Award for the Degree Final Project**\
Place: Barcelona (Spain) in 2018\
Institution: Catalan Society of Chemistry (SQC)

**Fellowship for a PhD in Science and Technology**\
Place: Tarragona (Spain) in 2018\
Institution: Catalan Institute of Chemical Research (ICIQ)

**Fellowship for the Master “Synthesis, Catalysis and Molecular Design”**\
Place: Tarragona (Spain) in 2017\
Institution: Catalan Institute of Chemical Research (ICIQ)

**Second Award for the Baccalaureate Project**\
Place: Reus (Spain) in 2014\
Institution: Reddis Private Foundation (REDDIS)
