---
title: Conferences and Outreach
subtitle: (updated August 28, 2024)
date: 2024-08-28
---

#### List of conferences

1. **Poster Presentation**\
Place: Riudoms (Spain) in **2018**\
Title: Unveiling a Photoinduced Hydrogen Evolution Reaction Mechanism via the Concerted Formation of Uranyl Peroxide\
Conference: INTECAT School


2. **Poster Presentation**\
Place: Tarragona (Spain) in **2019**\
Title: Unveiling a Photoinduced Hydrogen Evolution Reaction Mechanism via the Concerted Formation of Uranyl Peroxide\
Conference: ICIQ PhD Day III


3. **Poster Presentation**\
Place: Corvallis (United States) in **2019**\
Title: Unveiling a Photoinduced Hydrogen Evolution Reaction Mechanism via the Concerted Formation of Uranyl Peroxide\
Conference: Frontiers in Metal Oxide Cluster Science VI (FMOCS)


4. **Oral Communication**\
Place: Sitges (Spain) in **2020**\
Title: Performance of group additivity methods for predicting the stability of uranyl complexes\ 
Conference: Biannual conference of the Catalan Society of Chemistry (JIPC)\


5. **Flash Presentation**\
Place: online (Spain) in **2020**\
Title: Speciation and Self-Assembly Mechanism of Polyoxometalates\
Conference: ICIQ PhD Day IV


6. **Oral Communication**\
Place: Girona (Spain) in **2022**\
Title: Mecanismes de Formacio i Diagrames d’Especiacio dels Oxids Metallics Moleculars\
Conference: Biannual conference of the Catalan Society of Chemistry (JIPC)


7. **Oral Communication**\
Place: Rimini (Italy) in **2022**\
Title: POMSimulator: a Method for Predicting the Speciation and Self-Assembly of Polyoxometalates\
Conference: International Conference of Coordination Chemistry (ICCC)


8. **Oral Communication**\
Place: Barcelona (Spain) in **2023**\
Title: Predicting the aqueous speciation and self-assembly mechanisms of Mo, W, V, Nb and Ta polyoxometalates\
Conference: Catalan conference of inorganic chemistry and organometallics (RQIO)


9. **Oral Communication**\
Place: Indianapolis (United States) in **2023**\
Title: Ten years of interplay between theory and experiments\
Conference: ACS Spring Meeting


10. **Oral Communication**\
Place: Tarragona (Spain) in **2023**\
Title: Development of a Method for Predicting the Aqueous Speciation and Self-Assembly of Polyoxometalates\
Conference: Frontiers in Metal Oxide Cluster Science VII (FMOCS)


11. **Poster Presentation**\
Place: Bern (Switzerland) in **2023**\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Conference: Swiss Chemical Society Fall Meeting (SCS)


12. **Poster Presentation**\
Place: Zürich (Switzerland) in **2023**\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Conference: 59th Symposium on Theoretical Chemistry (STC)


13. **Poster Presentation**\
Place: Dübendorf (Switzerland) in **2023**\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Conference: Eawag Symposium


14. **Oral Communication**\
Place: Tarragona (Spain) in **2024**\
Title: An Open-Source Methodology for Predicting the Aqueous Speciation: from Polyoxometalates to Environmental Chemistry\
Conference: Electronic Structure Principles and Applications (ESPA)


15. **Poster Presentation**\
Place: Portland Maine (United States) in **2024**\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Conference: Gordon Research Conference (GRC) - Computational Chemistry


16. **Oral Communication**\
Place: Fribourg (Switzerland) in **2024**\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Conference: SCS Fall Meeting


17. **Poster Presentation**\
Place: Zürich (Switzerland) in **2024**\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Conference: ETH-Microsoft Workshop on High-Throughput Computational Chemistry


18. **Poster Presentation**\
Place: Dübendorf (Switzerland) in **2024**\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Conference: Eawag Symposium


19. **Seminar Talk and Workshop**\
Place: Groningen (Netherlands) in **2024**\
Title: Automated Reaction Network Exploration of Ozonation Processes in Water Treatment\
Invitation from Dr. Robert Pollice












