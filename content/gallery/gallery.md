---
title: Gallery
subtitle: (updated March 31, 2021)
date: 2020-06-23
---

In this section I will be posting the designs that I have made with
 [Blender](https://www.blender.org/). 



{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/img/toc_inorgchem2020.png" caption="TOC InorgChem2020">}}
  {{< figure thumb="-thumb" link="/img/pomsim.png" caption="Art Design for POMSimulator">}}
{{< /gallery >}}
{{< load-photoswipe >}}
