---
---

### Publications

In this section I will be posting the publications in which I have 
collaborated.

Petrus, E.; Bo, C. Unlocking Phase Diagrams for Molybdenum and Tungsten Nanoclusters and
Prediction of their Formation Constants. Preprint Available
at [chemrxiv.14292119](https://doi.org/10.26434/chemrxiv.14292119)

Petrus, E.; Segado, M.; Bo, C. Nucleation Mechanisms and Speciation of Metal
Oxide Clusters. Chem. Sci. 2020, 11, 8448-8456.
[doi.org/10.1039/D0SC03530K](https://doi.org/10.1039/D0SC03530K); Preprint Available
at [chemrxiv.12639269.v1](https://doi.org/10.26434/chemrxiv.12639269.v1)

Petrus, E.; Segado, M.; Bandeira, N. A. G.; Bo, C. Unveiling a Photoinduced
Hydrogen Evolution Reaction Mechanism via the Concerted Formation of Uranyl
Peroxide. Inorg. Chem. 2020, 59 (12), 8353–8360.
[doi.org/10.1021/acs.inorgchem.0c00757](https://doi.org/10.1021/acs.inorgchem.0c00757)

Petrus, E.; Bo, C. Performance of Group Additivity Methods for Predicting the
Stability of Uranyl Complexes. J. Comput. Chem. 2020, 41 (11), 1124–1129.
[doi.org/10.1002/jcc.26157](https://doi.org/10.1002/jcc.26157)

Martin, N. P.; Petrus, E.; Segado, M.; Arteaga, A.; Zakharov, L. N.; Bo, C.;
Nyman, M. Strategic Capture of the {Nb7} Polyoxometalate. Chem. - A Eur. J.
2019, 10580–10584. 
[doi.org/10.1002/chem.201902770](https://doi.org/10.1002/chem.201902770)
